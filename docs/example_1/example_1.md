# Example 1: Indoor Digital Twin

```{figure} ../../images/indoor_air_quality_front_image.jpg
---

name: indoor_air_quality_image-fig
---
An air quality sensor used by a member of the Platform Methodology team in their home office. Consisting of a Raspberry Pico, Pimoroni’s Pico Explorer, Pimoroni’s breakout sensors for temperature, humidity, pressure, volatile compounds and eCO2, LEDs and a small fan. 
```
<br/><br/>

## Required components:
- Raspberry Pi Pico (€4.95 or €7.95 with pre-soldered headers)
- MicroUSB to USB-A to Micro-B cable (~€1.25)
- Pimoroni BME280 Breakout Temperature, Pressure, Humidity Sensor (€15.95)
- Pimoroni SGP30 Air Quality Breakout (€19.95)
- Pimoroni Pico Explorer Base (€24.95)
- Various components, including: Three 330 Ω resistors, three LEDs, one 2.2kΩ resistor, a Transistor (NPN BC337), male-to-male wires, a Motor, a Terminal Connector, and a Fan blade (these components can be purchased separately although Kitronik’s Inventor’s Kit for BBC micro:bit will give you most of them in one package).
<br/><br/>

### Software required:
#### MicroPython 
MicroPython is a leaner implementation of Python 3 optimised for use on microcontrollers - a tiny computer on an integrated circuit chip. Therefore, this implementation of Python uses a subset of the standard libraries (e.g., JSON, time, sys, math, os, random, …) and incorporates other unique libraries (e.g., bluetooth, machine, …) that may be specific to a particular hardware, e.g., Raspberry Pi’s RP2040 chip specific package rp2. As this example uses several Pimoroni boards and sensors for ease we will be using MicroPython with Pimoroni Libraries, the latest version can be found and downloaded from [here](https://github.com/pimoroni/pimoroni-pico/releases).

```{note}
*To install Micropython to a Raspberry Pico remove the USB from the Pico, press and hold ‘BOOTSEL’, and whilst continuing to hold ‘BOOTSEL’ connect the USB of the Pico with a computer. The Pico should appear as a new drive called ‘RPI-RP2’, download the MicroPython firmware image and copy the download to the ‘RPI-RP2’ drive. The Pico will now restart. 
To write a programme to the Pico, connect the USB of a Pico with a computer (you do not need to hold down the BOOTSEL), write your programme, and save your programme as main.py to the Pico. Unplug from the computer and connect to an external power source and the programme should run*
```
<br/>

#### Thonny 
Thonny is an open-source integrated development environment (IDE) for Windows, Linux, and Mac OS that is useful for writing and uploading MicroPython to single board microcontrollers (such as a micro:bit or Raspberry Pico). This IDE can be downloaded [here](https://thonny.org/)

```{note}
*To have a programme run independent of your computer and the IDE save it to the pico as main.py, ensuring that the extension is .py. When connected to an external power supply it will automatically run.*
```