# Real-time data: Part 2

## Setting up our first sensor

### Recap

We've been writing a program to collect and collate three environmental variables (Temperature, Humidity, and Carbon Dioxide) and display the real-time data to an observer. The observer should be able to read off the values, so a display is required, and these values should be ‘real-time’ or as close to as possible. So far our program looks like this:

```{code-block} python
---
lineno-start: 1
---
import picoexplorer as display
from pimoroni_i2c import PimoroniI2C
from breakout_bme280 import BreakoutBME280
from breakout_sgp30 import BreakoutSGP30
import time
import utime

# Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# Set pins
i2c    = PimoroniI2C(**PINS_PICO_EXPLORER)
sgp30  = BreakoutSGP30(i2c)
bme    = BreakoutBME280(i2c)

width          = display.get_width()
height         = display.get_height()
display_buffer = bytearray(width * height * 2)
display.init(display_buffer)

white = display.create_pen(255, 255, 255)
black = display.create_pen(0, 0, 0)
red   = display.create_pen(255, 0, 0)

# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  90, 240, 3],[10, 110, 240, 5],'co2',      ' ppm', 0]
Temperature_display = [[10,  10, 240, 3],[10,  30, 240, 5],'temperature',' C', 2]
Humidity_display    = [[10, 170, 240, 3],[10, 190, 240, 5],'humidity'   ,' %', 2]


def show_data(variable_name,variable, 
            variable_unit, position, decimal_places = 0):
    """ Display data
    """ 
    
    # 0. Packages
    import picoexplorer as display
    
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])

## DATA
# read bme280 sensors
temperature, pressure, humidity = bme.read()

# pressure comes in pascals convert to the more manageable hPa
pressurehpa = pressure / 100

# get airquality sensors
air_quality     = sgp30.get_air_quality()
eCO2            = air_quality[BreakoutSGP30.ECO2]
TVOC            = air_quality[BreakoutSGP30.TVOC]

air_quality_raw = sgp30.get_air_quality_raw()
H2              = air_quality_raw[BreakoutSGP30.H2]
ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]
    
## VISUALISATION
# set the text to white
display.set_pen(white)
    
# add the temperature text
show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
# add the CO2 text
show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
# add the humidity text
show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

# update the display
display.update()
```

From line 51 onwards our device is now programmed to measure once, the three parameters of interest, and display the values of each. Not exactly useful if you are a sat in an office for a couple of hours. You want up-to-date real-time information. So we need to instruct the program to keep measuring continuously, for that we need to re-write our program to incorporate a loop. 

<br/>

### Loop

There are a couple types of loop: a **definite loop** which repeats a set of instructions a defined number of times and an **indefinite loop** which repeats without end whilst a condition is not met. Indefinite loops can veer off either intentionally or through error into an **infinite loop** when a condition can never be met. If we are making a time series we can instruct the program to keep measuring until some condition is met or forever (as long as power and storage space are not an issue). 


````{note} 
An example of a *definite loop* is as follows: 

```{code-block} python
---

---
for i in range(5):
    print('i is ',i)

print('done')

```
The variable *i* is defined by the program with the *range* function as a value in a series of numbers beginning at 0 up to, but not including, 5. It begins with zero because python is a **zero-indexed language** which is fancy way of saying it starts counting from 0 rather than 1. In Python the first line ends with a colon (:) instructing that the loop begins in the following. The instructions for the actions performed in the loop are **indented**, i.e., four empty spaces from the left, to allow Python to differentiate code belonging outside and inside this loop. If you put a loop within a loop, **nesting**, each new loop is indented four empty spaces further. Removing the indentation tells Python that this instruction is outside of a loop. Modifying the code to the following sets up an infinite *indefinite loop*:

```{code-block} python
---

---
i = 0                 # set up a 'counter'
while True:
    i += 1            # each time the loop iterates add 1
    print('i is ',i)

print('done')

```
Replacing the *for* with a *while* sets up a condition. As long as it is 'True' the loop will continue and as nothing within the loop changes it to 'False' the loop will run indefinitely. You might be asking yourself what variable we are referring to here that is being checked to see if it is True, it is essentially saying whilst True is True. To see how many times we have repeated we can set up a counter, during every loop we can add one to this counter (in python you can shorten i = i + 1 to i +=1). The statement outside of this loop will never be run. We can set a condition to be met that terminates our loop like so:

```{code-block} python
---

---
i = 0                 # set up a 'counter'
while True:
    i += 1            # each time the loop iterates add 1
    print('i is ',i)
    if i == 30:       # check if condition is met
        break         # terminate the loop using break

print('done')

```

````

<br/><br/>

To meet our goal our program needs to produce a continuous set of measurements and continuously update the display with them. Therefore, we need to incorporate an *indefinite loop*. By doing so we can also include a **calibration** step which is an act of configuration to produce a result within an acceptable range. The SGP30 sensor during its initilisation state returns fixed values of 400 ppm CO2eq and 0 ppb TVOC. Therefore, we should incorporate this into our loop:

```{code-block} python
---
lineno-start: 51

---

sgp30.start_measurement(False)
count = 0
while True:
    count += 1
   
    # get airquality sensor sgp30 data
    air_quality     = sgp30.get_air_quality()
    eCO2            = air_quality[BreakoutSGP30.ECO2]
    TVOC            = air_quality[BreakoutSGP30.TVOC]

    air_quality_raw = sgp30.get_air_quality_raw()
    H2              = air_quality_raw[BreakoutSGP30.H2]
    ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]

    # get temperature sensor bme280 data
    temperature, pressure, humidity = bme.read()
    
    # pressure comes in pascals convert to the more manageable hPa
    pressurehpa = pressure / 100
    
    if count == 30:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("Restarting measurement, waiting 15 secs before returning")
        sgp30.start_measurement(True)
        print("Measurement restarted, now read every second")
  
    display.clear()
    # drawing the LCD text
    display.set_pen(white)
    
    # add the temperature text
    show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
    # add the CO2 text
    show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
    # add the humidity text
    show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

    # time to update the display
    display.update()
    
    # wait one second before restarting loop
    time.sleep(1.0)


```


## Full code:
Our program should now meet our goal of providing continuous real-time observations of temperature, humidity, and CO2 levels in our office. In order to understand the code, including what inputs and outputs are necessary, it is a good practice to fully annotate your code. The full code is as follows:

<br/>

```{code-block} python
---


---
# 0 Import packages and libraries
import picoexplorer as display
from pimoroni_i2c import PimoroniI2C
from breakout_bme280 import BreakoutBME280
from breakout_sgp30 import BreakoutSGP30
import time
import utime

# 0.1 Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# 0.2 set pins
i2c    = PimoroniI2C(**PINS_PICO_EXPLORER)
sgp30  = BreakoutSGP30(i2c)
bme    = BreakoutBME280(i2c)

# 0.3 DISPLAY: 
width          = display.get_width()
height         = display.get_height()
display_buffer = bytearray(width * height * 2)
display.init(display_buffer)

#lets set up some pen colours to make drawing easier
white = display.create_pen(255, 255, 255)
black = display.create_pen(0, 0, 0)
red   = display.create_pen(255, 0, 0)

# Set Position and Values for displaying measurements
# variable2display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]
CO2_display         = [[10,  90, 240, 3],[10, 110, 240, 5],'co2',      ' ppm', 0]
Temperature_display = [[10,  10, 240, 3],[10,  30, 240, 5],'temperature',' C', 2]
Humidity_display    = [[10, 170, 240, 3],[10, 190, 240, 5],'humidity'   ,' %', 2]

# 0.4 FUNCTIONS
# make a function to display data

def show_data(variable_name,variable, 
            variable_unit, position, decimal_places = 0):
    """ Display data on LCD screen
    
    INPUT:
    variable_name  = str , name of variable
    variable       =     , values of variable
    variable_unit  = str , unit of the variable
    position       = list, positional data in a list of lists in the following format
                     [[position of var. name text],[position of var. value text]]
    decimal_places = num , number of decimal places to display for the value, default = 0

    OUTPUT:

    
    REQUIREMENTS:
    import picoexplorer as display
    
    EXAMPLE:
    
    # make a single variable with the following format
    # variable2display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]
    Temperature_display = [[10,  10, 240, 3],[10,  30, 240, 5],'temperature',' C', 2]
    show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    """
    # 0. Packages
    import picoexplorer as display
    
    # 1. Show 
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])


# 1. Measurements
# start, first 30 will be rejected and used to 'warm up the device'
sgp30.start_measurement(False)

count = 0         # initiate counter

while True:
    count += 1
   
    # get airquality sensor sgp30 data
    air_quality     = sgp30.get_air_quality()
    eCO2            = air_quality[BreakoutSGP30.ECO2]
    TVOC            = air_quality[BreakoutSGP30.TVOC]

    air_quality_raw = sgp30.get_air_quality_raw()
    H2              = air_quality_raw[BreakoutSGP30.H2]
    ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]

    # get temperature sensor bme280 data
    temperature, pressure, humidity = bme.read()
    
    # pressure comes in pascals convert to the more manageable hPa
    pressurehpa = pressure / 100
    
    if count == 30:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("Restarting measurement, waiting 15 secs before returning")
        sgp30.start_measurement(True)
        print("Measurement restarted, now read every second")
  
    display.clear()
    # drawing the LCD text
    display.set_pen(white)
    
    # add the temperature text
    show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
    # add the CO2 text
    show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
    # add the humidity text
    show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

    # time to update the display
    display.update()
    
    # wait one second before restarting loop
    time.sleep(1.0)

```

