# Real-time data

## Sensors
To understand a process, phenomenon, or physical asset we need to make observations, that is we need a sensor to acquire data. The first step then is to set up a sensor.

```{note}
A **sensor** is something that either detects or measures a physical, chemical, or biological property. From which it records, indicates, or responds to that property. Therefore, a sensor is a device from which a useable output can be obtained in response to a change in or specific measurand.
```

The sensor should be accurate, i.e., values obtained are close to their true value, and precise, i.e., replicate measurements are close to one another. Ideally to make inferences such data should capture the full, natural, range in conditions that a process, phenomenon, or physical asset experiences. For example, if we are interested in how to grow a tomato plant having data only when conditions are both dry and sunny will not allow us to make inferences beyond such conditions. 

```{note}
**Precision** and **Accuracy** are both measures of observational error, precision being the spread in measurements whilst accuracy the closest of the measurement value (e.g., bias or offsets) to its true value. Accuracy can sometimes be further defined in relation to the degree of precision, a measurement with low precision would also be low in accuracy.
```
<br/>

## Initial step: Plan ahead

Before we leap into plugging (in sensors) and playing it is useful to first 
map out what your project aims to do, the goal, and therefore what (technical, etc.) requirements are necessary to achieve that goal. Whilst our project is relatively simple it is prudent to become familiar with the steps necessary for larger projects. In larger projects the planning stage is both essential and more complex. For instance, you may need to plan developer time, materials, budgets, schedule testing and implementation phases, etc. etc. Planning also helps us to set and achieve short term goals, identify the 'minimum viable product', and to keep us on track. 

<br/>

The goal of this first iteration is to **collect** and **collate** three environmental variables (Temperature, Humidity, and Carbon Dioxide) and **display** the real-time data to an observer. The observer should be able to read off the values, so a display is required, and these values should be 'real-time' or as close to as possible. 

<br/>

## Setting up our first sensor

### Libraries

Plug in your Pico into the device in which you have downloaded the [Thonny IDE](https://thonny.org/). Create a new program, and begin writing some code by first importing the various libraries needed for the display and breakouts: 

```{code-block} python
---
lineno-start: 1
---
import picoexplorer as display
from pimoroni_i2c import PimoroniI2C
from breakout_bme280 import BreakoutBME280
from breakout_sgp30 import BreakoutSGP30
```
<br/>

These libraries contain classes and functions that have been written by other developers for a (specific) task. Alongside the libraries required for the physical computing components you will also need to import libraries associated with time, so as to be able to add delays between measurements:

```{code-block} python
---
lineno-start: 5

---
import time
import utime
```
<br/>

```{note}
Libraries in (Micro)Python can be imported in different ways, for instance the entire library (e.g., import time), the entire library 'as' a different usually abbreviated or simplified name (e.g., import numpy as np), or only particular functions or classes from the library (e.g., from breakout_bme280 import BreakoutBME280).
```

### Pins

Before controlling the sensors your program needs to set up the Pico's general-purpose input/output (GPIO) pins. These are the gold castellations and holes on the longer edges that either soldering or headers attach to. They connect the Pico to the world: 

```{code-block} python
---
lineno-start: 8

---
# Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# Set pins
i2c    = PimoroniI2C(**PINS_PICO_EXPLORER)
sgp30  = BreakoutSGP30(i2c)
bme    = BreakoutBME280(i2c)
```
<br/>

These lines tell the program which GPIO pins to used. Pins GP20 and GP21 are the default i2c pins for the Pico Explorer. I2C, or Inter-Integrated Circuit to give its full name is the protocol used to establish communication between two or more integrated circuits (ICs). The i2c variable is then used in the classes (BreakoutSGP30, BreakoutBME280) from the previously imported libraries (breakout_sgp30, breakout_bme280) associated with the two breakouts used in this example (SGP30 and BME280). 
<br/><br/>

### Display

Next we need to initialise the display:


```{code-block} python
---
lineno-start: 16

---
width          = display.get_width()
height         = display.get_height()
display_buffer = bytearray(width * height * 2)
display.init(display_buffer)
```
<br/>
And create some variables that can be quickly called to change the colour of the displayed text. To tell the display which colour to use requires a triple, here three whole numbers between 0 and 255 that gives values for red, green, and blue (RGB) respectively. To make specific colours the values of these three numbers varies, for instance to get white you would use (255,255,255) whilst for black you would use (0,0,0). Other colours are represented by variation in the specific combinations, e.g., red is (255,0,0):

```{code-block} python
---
lineno-start: 21

---
white = display.create_pen(255, 255, 255)
black = display.create_pen(0, 0, 0)
red   = display.create_pen(255, 0, 0)
```
<br/>

For now we will use the white for out text. This text will consist of the name of the variable measured (i.e., temperature, carbon dioxide, and humidity) as well as each variables current value. For each sensor measurement we need to assign it to a variable and relay that information to an obsever. We can program this with the following:  

```{code-block} python
---
lineno-start: 25

---
# GET DATA
# specific for breakout
air_quality    = sgp30.get_air_quality()               # get measurements
variable       = air_quality[BreakoutSGP30.ECO2]       # get variable

# SHOW DATA
# generic for display
position       = [[10,  90, 240, 3],[10, 110, 240, 5]] # define position of text on LCD
variable_name  = 'co2',                                # give the variable title/name
variable_unit  = ' ppm'                                # give the units of the variable
decimal_places =  0                                    # how many decimal places

# show the title
display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])

# show the value including its unit of measurement
display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])
```
<br/>

These ~20 lines only relay information regarding a single measurement, there are two more variables. You can envision that our program can quickly become bloated which has a negative impact upon readability. To reduce the amount of code and therefore enhance its readability we can make a function to perform repetitive actions. Lets re-write this portion of code.
<br/><br/>

First let us collate the information required for displaying each measurement (e.g., position, variable name, unit, etc.) into a list (anything between '[]' in python) of lists: 

```{code-block} python
---
lineno-start: 25

---

# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  90, 240, 3],[10, 110, 240, 5],'co2',      ' ppm', 0]
Temperature_display = [[10,  10, 240, 3],[10,  30, 240, 5],'temperature',' C', 2]
Humidity_display    = [[10, 170, 240, 3],[10, 190, 240, 5],'humidity'   ,' %', 2]
```
<br/>

Having collated the information in this way we can construct a function to do the tasks repetitively: 

```{code-block} python
---
lineno-start: 33

---
def show_data(variable_name,variable, 
            variable_unit, position, decimal_places = 0):
    """ Display data
    """ 
    
    # 0. Packages
    import picoexplorer as display
    
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])
```
<br/>

The first line (Line 33) defines the function ('def') and gives it a descriptive name ('show_data'), a series of variables between brackets, and ending with a colon (':'). The next line (Line 34) starts by indenting by four space, telling (micro)python where the function begins and ends. This line begins with three speech marks (""") denoting that this is also a comment, anything between the first triple of speech marks and second triple will not be acted upon, useful when you want to add user instructions (see note below). The subsequent lines (Line 38 to Line 47) deal with the specific tasks the function is required to carry out.

```{note}
Code, Functions, and Algorithms work well when they are well documented. We can add rich metadata that can be useful for new and existing users of our function to (re)use, modify, or adapt. Our approach is to add statements that address (at least) the *RATIONALE*, *INPUT*, *OUTPUT*, *REQUIREMENTS*, and *EXAMPLE* of use. As per the following template: 

```{code-block} python
---


---
def function(variable1, variable2 = 1):
    """ short rationale
    INPUT:
        variable1 = what it is, type, 
        variable2 = what it is, type, default values = 1
    
    OUTPUT:
        output = what is returned by the function, here adding the two variables

    REQUIREMENTS:
        import package # functions or packages required
    
    EXAMPLE:
        function(variable1,varible2) #example of how to use

    """ 
    import package

    output = variable1 + variable2

    return output
```
<br/>


### Sensor reading

Having prepared the code and functions necessary to display the data it is time to 'measure' the different parameters:

```{code-block} python
---
lineno-start: 51

---
# read bme280 sensors
temperature, pressure, humidity = bme.read()

# pressure comes in pascals convert to the more manageable hPa
pressurehpa = pressure / 100

# get airquality sensors
air_quality     = sgp30.get_air_quality()
eCO2            = air_quality[BreakoutSGP30.ECO2]
TVOC            = air_quality[BreakoutSGP30.TVOC]

air_quality_raw = sgp30.get_air_quality_raw()
H2              = air_quality_raw[BreakoutSGP30.H2]
ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]
    
```
<br/>
Now we can visualise the measurement using our previously defined function 'show_data':

```{code-block} python
---
lineno-start: 64

---
# set the text to white
display.set_pen(white)
    
# add the temperature text
show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
# add the CO2 text
show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
# add the humidity text
show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

# update the display
display.update()
```
<br/>

### Our program so far

Our program now reads like so:
```{code-block} python
---

---
import picoexplorer as display
from pimoroni_i2c import PimoroniI2C
from breakout_bme280 import BreakoutBME280
from breakout_sgp30 import BreakoutSGP30
import time
import utime

# Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# Set pins
i2c    = PimoroniI2C(**PINS_PICO_EXPLORER)
sgp30  = BreakoutSGP30(i2c)
bme    = BreakoutBME280(i2c)

width          = display.get_width()
height         = display.get_height()
display_buffer = bytearray(width * height * 2)
display.init(display_buffer)

white = display.create_pen(255, 255, 255)
black = display.create_pen(0, 0, 0)
red   = display.create_pen(255, 0, 0)

# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  90, 240, 3],[10, 110, 240, 5],'co2',      ' ppm', 0]
Temperature_display = [[10,  10, 240, 3],[10,  30, 240, 5],'temperature',' C', 2]
Humidity_display    = [[10, 170, 240, 3],[10, 190, 240, 5],'humidity'   ,' %', 2]


def show_data(variable_name,variable, 
            variable_unit, position, decimal_places = 0):
    """ Display data
    """ 
    
    # 0. Packages
    import picoexplorer as display
    
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])

## DATA
# read bme280 sensors
temperature, pressure, humidity = bme.read()

# pressure comes in pascals convert to the more manageable hPa
pressurehpa = pressure / 100

# get airquality sensors
air_quality     = sgp30.get_air_quality()
eCO2            = air_quality[BreakoutSGP30.ECO2]
TVOC            = air_quality[BreakoutSGP30.TVOC]

air_quality_raw = sgp30.get_air_quality_raw()
H2              = air_quality_raw[BreakoutSGP30.H2]
ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]
    
## VISUALISATION
# set the text to white
display.set_pen(white)
    
# add the temperature text
show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
# add the CO2 text
show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
# add the humidity text
show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

# update the display
display.update()
```

So are we done? 


:::::{admonition} Have we achieved our goals? Have a think!

````{panels} 
```{dropdown} Yes. Lets go and have a cup of tea to celebrate
Probably hold off on that cuppa. Go and have a look at the Goals again and consider what our code does. 
```
---
```{dropdown} Not yet, but we are close.
Indeed our goal was to provide (near) real-time observations of the three variables. The program currently provides a single measurement as soon as it is run. As time progresses (as it is want to do) the value may change, any decisions made will be using data that may not be useful or incorrect. We need a feature to keep the value up to date.
```

````
:::::

