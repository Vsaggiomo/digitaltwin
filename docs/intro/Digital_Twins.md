# Digital Twins

## What is a Digital Twin?

Digital Twins are an innovative technology that - as part of the fourth industrial revolution (Industry 4.0) [13] - builds upon the previous iterations’ expansion of digital processes and services by aiming to provide greater virtual-physical connection and automation [14,15]. Current proponents of Digital Twins argue that the technology promises to offer real-time remote monitoring and control; greater efficiency and safety; better predictive maintenance and scheduling; more efficient and informed decisions; greater personalization of products and services; and better documentation and communication [16,17]. But what are Digital Twins? 
<br/><br/>

```{figure} ../../images/DT.jpg
---

name: dt_simple_figure-fig
---
Simplified schematic of the processes involved within a Digital Twin. Concept modified from
```
<br/><br/>

There are numerous definitions of what Digital Twins are and these generally feature a number of commonalities (physical, virtual, information exchange, modelling, simulation, and or decision-making). The concept of Digital Twins has its basis in **product lifecycle management** (PLM), the process by which a product (car, airplane, toy, etc.) is managed from its inception, development, deployment, operation and eventual decline [19–22]. In PLM a Digital Twin could act as the virtual counterpart to the real-world entity or phenomenon, able to store, link and visualise the data generated throughout the product’s lifecycle. With such a virtual counterpart the user, technician or engineer would be able to gain as much information as if they were really inspecting the physical asset, or in some instances even more information (e.g., an airplane in flight; a city transport network). While Digital Twins in many cases involve (constructed) physical objects based on a product life cycle, the concept can also be applied to living organisms or parts of organisms, that also have a life cycle.
<br/><br/>

```{figure} ../../images/DT_advanced.jpg
---

name: dt_advanced_figure-fig
---
Components and processes of a Digital Twin. Schema developed by the Digital Twin Methodology Platform Exploratory Project on Metadata.
```
<br/><br/>

A digital twin refers to the process of coupling an existing physical asset, or one that could conceivably exist, to a virtual replica ({numref}`dt_simple_figure-fig`). For a digital twin, this connection is bidirectional, information flows both ways to enable or initiate a decision-making process ({numref}`dt_advanced_figure-fig`) producing actionable knowledge. Actionable knowledge is the term for the coupling of Science and Management [9]. If science provides generalisations to describe or explain particular phenomena, management is about bringing about intended consequences through directed action. Actionable knowledge is knowledge that can therefore, (i) provide the specific conditions/actions to create a particular consequence beyond the settings in which it was first created; (ii) the proposition should lead to the intended consequences; and (iii) a generalisation that can be applied to individuals as well as to groups.
<br/><br/>

The physical asset is the real world entity, assets that have been considered for ‘twinning’ by a Digital twin range in scale from the microbial all the way up to the planet [3]. Digital twins can be a singular component (e.g., a pump), an individual (e.g., a car) or an aggregate/ group of individuals (e.g., a company’s transport fleet). It is important to note that a digital twin does not impose any exclusivity, so the same physical asset can by represented by more than one digital twin [4] all of which can share information. 
<br/><br/>

In the virtual environment the physical asset is twinned by a virtual representation that should provide the user with all the information they could obtain were they to physically inspecting the asset in the real-world. Some definitions of Digital Twins implicitly state that this necessitates the virtual representation being a 3D model of the asset. In reality the virtual representation is comprised (Figure 2) of a visualisation component (what the user see’s) and a processing component (how the raw sensor data and ‘historic’ data are used) which does not necessarily require a 3D model. The visualisation components can range from simple iconography, a dashboard, graphs and graphics, or a 3D model. Whereas the processing components can range from data, a data-model, physics-based model, artificial intelligence, or some combination of all of these. Ultimately the form the virtual representation takes will be dependent upon the knowledge of the user and the purpose of the Digital Twin.
<br/><br/>

The two-way flow of information differentiates Digital twins from more conventional static process models and decision support tools by incorporating the most up to date knowledge about the status of the physical asset. 

<br/><br/>

```{note}
**Product Lifecycle Management** - Original term for the concept used by Grieves that would eventually become Digital Twins
```