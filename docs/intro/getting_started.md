# Getting started
In this example section we will cover what you need to have in order to follow along with the examples outlined in this book. For each example a list of components and software required is given. As well as how to build these devices.
<br/><br/>

See the sections below to get started.

- {Example 1: Indoor Digital Twin}`example 1.md`