# Design process

Digital Twins should be considered as both **goal orientated** and **user centered** from the onset of an **iterative design process**. In the following we will elaborate what we mean by these terms.
<br/><br/>

## Goal orientated 
Goal orientated, or more formally 
Goal-driven Software Development Process (GDP) [(Schnabel and Pizka, 2006)](https://doi.org/10.1109/SEW.2006.21), is about first identifying **goals** prior to setting **requirements**. Goals and requirements may sound similar but they differ in that goals are the *why* whilst requirements are the *what*. Goals reflect the overall aim of a project with clearly defined outcomes. Whereas, requirements focus on details. For example, what parameters, what assumptions, what techniques, what technology, what software... and so forth. At the early onset of a project it can be unrealistic to set requirements prior to technical implementation, as it is only during this stage that knowledge gaps and inconsistencies emerge. Hence by first identifying goals with stakeholders and developers and then formalizing these into processes before distributing tasks use cases can be tailored to requirements according to the goals. 
<br/><br/>

## User centered design (UCD)
The user, their needs, wants, and desires should be the focal point of the design process. This ensures that the outputs meets their needs, wants, and requirements. User testing is an integral part of of the user centered design process and should take place throughout the design process not just after completion of the final product within an iteration.
<br/><br/>

## Iterative design process
The iterative design process is a methodology based upon a (continuous) cycle of designing, building, testing, and learning. It can be a powerful methodology, breaking down large projects into bitesize chunks that have continuous testing and feedback. Therefore, it is the basis for many (software development) project management styles.
<br/><br/>

```{note}
**Iterative design process** - The terminology of the different design stages differs, for instance: prototyping, testing, analyzing, and refining or formulate, test, evaluate. In essence they reflect similar stages: an initial designing stage; a construction phase, and then a phase of evaluating the output (e.g., identifying improvements, or next steps).
```

## Narrative approach
A goal-orientated, user-centered, iterative design process is central to building and developing a Digital Twin. Therefore, for each of the examples outlined in this book we will utilise a narrative approach to highlight these aspects. 
<br/><br/>

