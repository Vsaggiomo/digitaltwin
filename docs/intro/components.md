# Components 

## Aspects of a Digital Twin 
The Methodology Platform of the WUR Digital Twin investment theme consider that a Digital Twin is *a coupling of the physical and virtual realities via a two-way flow of information that leads to some form of actionable knowledge ({numref}`dt_simple_figure-fig`)*. Such a system is comprised of five components: (i) a physical asset or system that is; (ii) connected by a flow of information (‘data streams’) from which it can be; (iii) virtually represented, so that; (iv) information can flow from the virtual to the physical as; (v) a form of actionable knowledge (‘actions’). 
<br/><br/>

## Physical Asset
The physical asset represented by a virtual counterpart in the virtual world will be dependent upon the **use case** and can therefore conceivably range in size from the microbial up to the planetary scale [23]. The physical asset can be an individual or an amalgamation, and need not currently exist as long it can in principle. For example, a Digital Twin could be used in the construction and development of genetically modified organisms, in such a scenario the twin could 'prototype' the organism prior to it being a thing in the real world. 
<br/><br/>

## Communication
A Digital Twin is a two-way coupling between an actual or theoretically plausible process (e.g., a farm), a phenomenon (e.g., a river), or an asset (e.g., a cow, or human) in the physical world with a virtual copy. Sensors connect the physical asset to its digital counterpart by providing data at the relevant spatial and temporal scales. This communication is two-way therefore there should be a connection between the virtual and physical world (see Actionable Knowledge and Control below).
<br/><br/>

## Simulation
The Digital Twin is endowed with a set of models or algorithms that incorporate the newly acquired data to simulate the behaviour of the system under alternative scenarios. These simulations can be used to evaluate the optimal intervention strategies to obtain the predefined goal or to let the system operate within predefined set points.
<br/><br/>

## Actionable Knowledge and Control
 The coupling between the Digital Twin and the physical asset is completed through information returning from the virtual to the physical world. This information may take the form of knowledge, i.e., informing individuals interested in the physical asset of its current status and any potential predictions regarding its state via visualisation.
 <br/><br/>
 
  Or this information can take the form of actions via controllers operated by the digital system that effectively steers the behaviour of the system towards an end goal. For instance, a Greenhouse may increase the temperature, add nutrients, or reduce the amount and frequency of water added.
 <br/><br/>

 ## Summary
 Thus, key elements of the Digital Twin are the sensors and the controllers together with the models, simulations environments and data and metadata streams ({numref}`dt_advanced_figure-fig`). 
<br/><br/>

Let us consider how this works in practice...