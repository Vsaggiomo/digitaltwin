#include <SPI.h>
#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "Arduino";     // your network SSID (name)
char pass[] = "arduinoo";    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key index number (needed only for WEP)

int status = WL_IDLE_STATUS;

// InfluxDB v2 server url, e.g. https://eu-central-1-1.aws.cloud2.influxdata.com (Use: InfluxDB UI -> Load Data -> Client Libraries)
#define INFLUXDB_URL "http://ssb1.wurnet.nl"
// InfluxDB v2 server port
#define INFLUXDB_PORT 8086
// InfluxDB v2 server or cloud API token (Use: InfluxDB UI -> Data -> API Tokens -> <select token>)
#define INFLUXDB_TOKEN "6BJlBAwcFkwQo10RgtkHsPrQCBoex8-EIVJH2ngG6YiaXi4XliTj1p9dJW5zkby5qb4Sm4My4cqYbNnoYdIyaQ=="
// InfluxDB v2 organization id (Use: InfluxDB UI -> User -> About -> Common Ids )
#define INFLUXDB_ORG "WUR"
// InfluxDB v2 bucket name (Use: InfluxDB UI ->  Data -> Buckets)
#define INFLUXDB_BUCKET "DigitalTwin"

// Your Domain name with URL path or IP address with path
//String serverName = String(INFLUXDB_URL) + ":" + String(INFLUXDB_PORT) + "/api/v2/write";

WiFiClient wifi;
HttpClient client = HttpClient(wifi, INFLUXDB_URL, INFLUXDB_PORT);

void setup() {
  
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }
  
  String fv = WiFi.firmwareVersion();
  Serial.println("Current firmware version: " + fv);
  
  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();
}

void loop() {
  Serial.println("Starting loop");

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("WIFI connected");

    Serial.println("Begin POST request");
    client.beginRequest();
    client.post("/api/v2/write?org=WUR&bucket=DigitalTwin");
    client.sendHeader("Authorization", "Token LFgExBd-HiUrEh7uggh67LSa1SQrvWkdsYM4ceO9071qJO4d1UqdM9jt1uSXuzCpIV-aW8WQNDCCioRdLcGtAA==");
    client.sendHeader("Content-Type", "text/plain; charset=utf-8");
    client.sendHeader("Accept", "application/json");
    client.beginBody();
    client.print("'airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615'");
    client.endRequest();
    Serial.println("End POST request");

    // read the status code and body of the response
    int statusCode = client.responseStatusCode();
    String response = client.responseBody();
  
    Serial.print("Status code: ");
    Serial.println(statusCode);
    Serial.print("Response: ");
    Serial.println(response);
  
    Serial.println("Wait five seconds");
    delay(5000);

//  
//    // read the status code and body of the response
//    int statusCode = client.responseStatusCode();
//    Serial.print("Status code: ");
//    Serial.println(statusCode);
//    String response = client.responseBody();
//    Serial.print("Response: ");
//    Serial.println(response);
//  
//    Serial.println("Wait 30 seconds");
//    delay(30000);
//
////    curl -i -XPOST http://192.168.2.1:8086/api/v2/write\?org\=DigitalTwin\&bucket\=DigitalTwin --header "Authorization: Token 45dgOWQdNYRScvUvoG3On0mRG17XOtiGcHYzIeXHZBtYPV4KVSMYDHOPn6Liw0BzYF3e7r5QsUrcty9HPxsN4w=="  
////    --header "Content-Type: text/plain; charset=utf-8"  --header "Accept: application/json"  
////    --data-binary 'airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615'
//
////    client.post(serverName.c_str(), "someSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615");
//
////    Serial.println("Exit code: " + client.exitValue());
////    Serial.println("Response: " + client.getResponseHeaders());
////    delay(1000);
//      
//      // Data to send with HTTP POST
////      String httpRequestData = "api_key=tPmAT5Ab3j7F9&sensor=BME280&value1=24.25&value2=49.54&value3=1005.14";
//      // Send HTTP POST request
////      String httpRequestData = "--data-binary 'airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615'";
////      int httpResponseCode = http.POST(httpRequestData);
//
////      Serial.print("HTTP Response code: ");
////      Serial.println(httpResponseCode);
//        
//      // Free resources
////      http.end();
  }
//  
//  Serial.println(client.connect(INFLUXDB_URL, INFLUXDB_PORT));
//  
//  // Post command
//  String post = "POST " + String(INFLUXDB_URL) + ":" + String(INFLUXDB_PORT) + "/api/v2/write?org=" + String(INFLUXDB_ORG) + "&bucket=" + String(INFLUXDB_BUCKET);
//  post += " --header \"Authorization: Token " + String(INFLUXDB_TOKEN) + "\" ";
//  post += " --header \"Content-Type: text/plain; charset=utf-8\" ";
//  post += " --header \"Accept: application/json\" ";
//  post += " --data-binary 'airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615'";
//
//  Serial.println(post);
//  client.println(post);

  delay(10000);
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
